This program solves the Fibonacci prime variant of the FizzBuzz problem in Python. Given a value of N, it generates the Fibonacci numbers from F(1) to F(N), printing:

* "Buzz" when F(N) is divisible by 3.
* "Fizz" when F(N) is divisible by 5.
* "FizzBuzz" when F(N) is divisible by 15.
* "BuzzFizz" when F(N) is prime. 
* the value of F(N) otherwise.

# Method #

This program takes advantage of the fact that, for any N except 4, F(N) can be prime only if N is. This allows the program to quickly cull values of F(N) by performing a naive primality test on the (much smaller) N first. 

To test F(N) itself, this program uses the Miller-Rabin probabilistic test instead of the much slower but deterministic naive approach. Because this is a probabilistic test, there is a small chance that a composite will be misclassified as prime. However, using 5 bases, the chance of this happening is only 4**(-5), less than one in a thousand. This false positive rate can be controlled by using more bases, if more accuracy is needed.

On my laptop, it takes about 5 minutes to run for N=10,000 using this method.

# Requirements #

This program works with either Python 2.7 or 3.5. The only (optional) dependency is pytest, for the unit tests. If you don't have pytest installed, you can either install it with pip:


```
#!bash

pip install pytest
```

or execute the test command with the setup script (from the root of the source directory) to get setuptools to install it and run the tests for you:


```

python setup.py test
```


# Installation #

To install the program, navigate to the root directory and run


```
#!bash

python setup.py install

```

You can also run the program as a standalone script without installing. See below for more information.

# Usage #

If you installed the program into your path, you can run:


```
#!bash

fizzbuzz 10
```

To run the program for N=10. If you prefer, you can also execute the `fizzbuzz-runner.py` script without installing:


```
#!bash

python fizzbuzz-runner.py 10
```

You must be in the root of the source directory for this to work. 


# Tests #

The unit tests are designed to work with pytest. If you have pytest installed, you can navigate into the `test` directory and execute:


```
#!bash

py.test
```

If you don't have pytest installed already, see the installation instructions under "Requirements" above.

# References #

I found the following resources useful in completing this project:

https://en.wikipedia.org/wiki/Fibonacci_prime

https://en.wikipedia.org/wiki/Miller-Rabin_primality_test

https://en.wikipedia.org/wiki/Primality_test