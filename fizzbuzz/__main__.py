"""

This allows for direct execution of the fizzbuzz directory as a
Python script from the source root directory:

python -m fizzbuzz 100

"""


from .fizzbuzz import main
main()
