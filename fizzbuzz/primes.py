'''

This file contains functions for testing whether a given number is prime.

'''

import random


def sieve(N):
    '''

    This implements the Sieve of Eratosthenes for rapidly
    finding all the prime numbers less than N. See
    https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes for more
    information. This function uses O(N) space and doesn't implement
    segmenting, so don't try to use it for N too large to fit in memory.

    Parameters
    ----------

        N : integer
            Find all the primes less than this number.

    Returns
    -------

        A length-N list, where the ith entry (0-based) is True if i is
        prime and False otherwise.

    Example
    -------

    >>> sieve(10)
    [False, False, True, True, False, True, False, True, False, False]

    '''
    nums = list(range(N))
    nums[1] = 0
    for prime in nums:
        if prime < 2:
            continue
        elif prime * prime > N:
            break
        for i in range(prime**2, N, prime):
            nums[i] = 0
    return [num > 0 for num in nums]


def is_prime(n):
    '''

    This is a simple and inefficient deterministic primality test. In
    the context of finding Fibonacci primes, it should only be used to
    test the *indices* for primality, not the Fibonacci numbers themselves.

    Please see https://en.wikipedia.org/wiki/Primality_test for more
    information. This test uses the fact that any prime number greater
    than 3 can be written as 6*k + 1 or 6*k - 1.

    Parameters
    ----------

        n : integer

    Returns
    -------

        A boolean value indicating whether the number is prime or not.

    Examples
    --------

    >>> is_prime(7)
    True

    >>> is_prime(10)
    False

    >>> is_prime(1)
    False

    >>> is_prime(0)
    False

    >>> is_prime(-1)
    False


    '''

    #  if n is one or less, it is not prime
    if n <= 1:
        return False

    #  special case both 2 and 3
    elif n <= 3:
        return True

    #  discard multiples of 2 and 3
    elif n % 2 == 0 or n % 3 == 0:
        return False

    #  step through in multiples of 6, checking 6*k-1 and 6*k+1
    i = 5
    while i * i <= n:
        if n % i == 0:
            return False
        if n % (i + 2) == 0:
            return False
        i += 6

    return True


def is_prob_prime(n, num_bases=5):
    """

    The function implements the Miller-Rabin test for primality.
    See https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
    for more information.

    Miller-Rabin is a probabilistic test. If it returns False, the given
    number is definitely not prime. If it returns True, the given number
    is very likely to be prime, but this is not guaranteed. If n is
    composite, the probability that it will be declared prime by the test
    is 4**(-num_bases).

    Parameters
    ----------

        n : an integer greater than 3
            The number that will be tested for primality

        num_bases : integer, optional
            The number of bases that will be used to test for compositeness.
            Default is 5.

    Returns
    -------

        Boolean. If False, the number is composite. If True, the number is
        *probably* prime.

    Examples
    --------

    >>> is_prob_prime(17)
    True

    >>> is_prob_prime(10)
    False

    >>> is_prob_prime(19134702400093278081449423917)
    True

    """

    #  Miller-Rabin does not work for very small n, special case them here
    if n <= 3:
        return is_prime(n)

    #  n is composite if even
    if n % 2 == 0:
        return False

    #  write n - 1 as 2**r * d
    r = 0
    d = n - 1
    while True:
        quotient, remainder = d // 2, d % 2
        if remainder == 1:
            break
        r += 1
        d = quotient

    #  randomly select a base, test whether it is a witness to the
    #  compositeness of n.
    for _ in range(num_bases):
        a = random.randint(2, n - 2)
        x = pow(a, d, n)
        if x == 1 or x == n - 1:
            continue
        known_composite = True
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == 1:
                return False
            if x == n - 1:
                known_composite = False
                break
        if known_composite:
            return False
    return True
