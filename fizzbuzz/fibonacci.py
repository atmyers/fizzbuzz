'''

This file defines functions for generating the Fibonacci numbers and for
deciding whether a given Fibonacci number is prime or not.

'''

from .primes import \
    is_prime, \
    is_prob_prime


def fibonacci(N):
    '''

    This is a generator function that produces the first N Fibonacci numbers:
    1, 1, 2, 3, ... F(N-1), F(N).

    Parameters
    __________

        N : integer
            The index of the last Fibonacci number that will be generated.

    Example
    -------

    >>> [fn for fn in fibonacci(10)]
    [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

    '''
    current, prev, counter = 1, 1, 0
    while True:
        if (counter >= N):
            return
        yield prev
        current, prev = current + prev, current
        counter += 1


def is_fibonacci_prime(n, fn, num_bases=5):
    '''

    This function tests whether the nth Fibonacci number is prime or not.
    It relies on the fact that, except for the case of n == 4, fn can only
    be prime if n is. We can use this to quickly screen the Fibonacci numbers
    by testing whether the (much smaller) index is prime first.

    Strictly speaking, this is only a probabilistic test; it will sometimes
    return false positives (i.e., primes that are actually composite). The
    chance that a composite number will be categorized as prime is
    4**(-num_bases).

    Parameters
    ----------

        n : integer
            the index of the Fibonacci number being tested.

        fn : integer
            the nth Fibonacci number

        num_bases : integer, optional
            the number of bases that will be tried in the Miller-Rabin
            primality check. Default is 5.

    '''

    if n <= 4:
        return is_prime(fn)
    elif is_prime(n):
        return is_prob_prime(fn, num_bases)
    return False
