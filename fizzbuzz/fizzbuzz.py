'''

This file defines the functions used to handle the FizzBuzz logic,
as well as the main entry point into the program.

'''

import sys
from .fibonacci import \
    fibonacci, \
    is_fibonacci_prime


def _get_output_string(n, fn):
    '''

    This function gets the output string associated with a given
    number in the Fibonacci sequence.

    Parameters
    ----------

        n : integer
            The index of Fibonacci number
        fn : integer
            The Fibonacci number itself

    Returns
    -------

        A string that will be printed to the string for that value
        of F(n).

    Examples
    --------

    >>> get_output_string(1, 1)
    "1"

    >>> get_output_string(3, 2)
    "BuzzFizz"

    >>> get_output_string(8, 21)
    "Buzz"

    >>> get_output_string(10, 55)
    "Fizz"

    '''

    if is_fibonacci_prime(n, fn):
        return "BuzzFizz"
    elif fn % 15 == 0:
        return "FizzBuzz"
    elif fn % 5 == 0:
        return "Fizz"
    elif fn % 3 == 0:
        return "Buzz"
    else:
        return str(fn)


def fizzbuzz(N):
    '''

    This function solves the "Fibonacci Prime" variant of the FizzBuzz
    problem. Given N, it generates the first N Fibonacci numbers F(n)
    (starting from n = 1) , printing:

    "Buzz" when F(n) is divisible by 3.
    "Fizz" when F(n) is divisible by 5.
    "FizzBuzz" when F(n) is divisible by 15.
    "BuzzFizz" when F(n) is prime.

    Otherwise, it prints the value of F(n).

    Conditions at the bottom of the list are considered "stronger" than
    conditions towards the top. That is, for n = 4, F(n) = 3, it prints
    "BuzzFizz" because F(n) is prime, not "Buzz" because F(n) is 3.

    '''

    f = fibonacci(N)
    for n, fn in enumerate(f, 1):
        print(_get_output_string(n, fn))


def main():
    '''

    The main entrypoint into the program.

    '''
    try:
        assert(len(sys.argv) == 2)
        N = int(sys.argv[1])
    except(ValueError, AssertionError):
        sys.exit('Usage: %s <integer>' % sys.argv[0])
    fizzbuzz(N)
