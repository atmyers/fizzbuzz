#!/usr/bin/env python

"""

This is a simple wrapper script that allows for direct execution of the
program from the source root directory without installation.

"""


from fizzbuzz.fizzbuzz import main


if __name__ == '__main__':
    main()
