from setuptools import setup


version = '0.0.1'


with open("README.md", "rb") as f:
    long_descr = f.read().decode("utf-8")


setup(
    name = "fizzbuzz",
    packages = ["fizzbuzz"],
    entry_points = {
        "console_scripts": ['fizzbuzz = fizzbuzz.fizzbuzz:main']
        },
    version = version,
    description = "Python program to compute the Fibonacci Prime version of FizzBuzz.",
    long_description = long_descr,
    author = "Andrew Myers",
    author_email = "atmyers2@gmail.com",
    url = "http://https://bitbucket.org/atmyers/FizzBuzz",
    setup_requires=['pytest-runner'],
    tests_require=['pytest']
    )
