'''

Tests of the Fibonacci functions.

'''

import os
import sys
from fizzbuzz.fibonacci import \
    fibonacci, \
    is_fibonacci_prime

sys.path.insert(0, os.path.abspath('..'))


def test_fibonacci_known():
    '''

    Compare the first 12 generated values against the known sequence.

    '''

    fibonacci_sequence_known = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]
    assert(list(fibonacci(12)) == fibonacci_sequence_known)


def test_fibonacci_recurrence():
    '''

    Compare the first 500 generated values against the recurrence relation.

    '''

    prev = 1
    current = 1
    f = fibonacci(500)
    for fn in f:

        # skip the first to get it started
        if (fn == 1):
            continue

        assert(prev + current == fn)
        prev = current
        current = fn


def test_fibonacci_primes():
    '''

    This tests the is_fibonacci_prime function against a list of the known
    prime locations.

    Note: because is_fibonacci_prime uses a probabilistic method, there is a
    small chance that this test could fail even if everything is working
    correctly. A better approach might be to test where the *rate* of false
    positives differs significantly from the expected rate for the Miller-Rabin
    test.

    '''

    fib_prime_indices = [3, 4, 5, 7, 11, 13, 17, 23, 29, 43, 47, 83, 131,
                         137, 359, 431, 433, 449]

    f = fibonacci(500)
    for n, fn in enumerate(f, 1):
        prime = is_fibonacci_prime(n, fn)
        if n in fib_prime_indices:
            assert(prime)
        else:
            assert(not prime)
