'''

Tests for the overall fizzbuzz program.

'''

import os
import sys
from fizzbuzz.fibonacci import \
    fibonacci
from fizzbuzz.fizzbuzz import \
    _get_output_string

sys.path.insert(0, os.path.abspath('..'))


def test_fizzbuzz():
    '''

    Compare the result of the fizzbuzz function to the expected answer
    (worked out manually for the first 10 Fibonacci numbers).

    '''

    first_ten_answers = ["1", "1", "BuzzFizz", "BuzzFizz", "BuzzFizz", "8",
                         "BuzzFizz", "Buzz", "34", "Fizz"]

    f = fibonacci(10)
    for n, fn in enumerate(f, 1):
        assert(_get_output_string(n, fn) == first_ten_answers[n-1])
