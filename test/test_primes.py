'''

Tests of the primality checking functions.

'''

import os
import sys
from fizzbuzz.primes import \
    sieve, \
    is_prime, \
    is_prob_prime

sys.path.insert(0, os.path.abspath('..'))


def test_sieve():
    '''

    Tests the sieve function agaist the first 12 known primes.

    '''
    sieve_primes = sieve(38)
    known_primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
    for num in range(37):
        if (num in known_primes):
            assert(sieve_primes[num])
        else:
            assert(not sieve_primes[num])


def test_is_prime():
    '''

    Tests the is_prime function against the results of the sieve,
    over the range 0 to 99.

    '''
    N = 100
    primes = sieve(N)
    for num in range(N):
        assert(primes[num] == is_prime(num))


def test_is_prob_prime():
    '''

    Tests the is_prob_prime function against the results of the sieve,
    over the range 0 to 99.

    Note: because is_fibonacci_prime uses a probabilistic method, there is a
    small chance that this test could fail even if everything is working
    correctly. A better approach might be to test where the *rate* of false
    positives differs significantly from the expected rate for the Miller-Rabin
    test.

    '''
    N = 100
    primes = sieve(N)
    for num in range(N):
        assert(primes[num] == is_prob_prime(num))
